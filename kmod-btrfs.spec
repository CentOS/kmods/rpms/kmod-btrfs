%global pkg btrfs

%global kernel 5.14.0-284.11.1.el9_2
%global baserelease 1

%global debug_package %{nil}

%global __spec_install_post \
  %{?__debug_package:%{__debug_install_post}} \
  %{__arch_install_post} \
  %{__os_install_post} \
  %{__mod_compress_install_post}

%global __mod_compress_install_post find %{buildroot}/lib/modules -type f -name \*.ko -exec xz \{\} \\;


Name:             kmod-%{pkg}
Epoch:            1
Version:          %(echo %{kernel} | sed 's/-/~/g; s/\.el.*$//g')
Release:          %{baserelease}%{?dist}
Summary:          Btrfs filesystem support

License:          GPLv2
URL:              https://www.kernel.org/

Patch0:           source-git.patch

ExclusiveArch:    x86_64 aarch64 ppc64le

BuildRequires:    elfutils-libelf-devel
BuildRequires:    gcc
BuildRequires:    kernel-rpm-macros
BuildRequires:    kmod
BuildRequires:    make
BuildRequires:    redhat-rpm-config
BuildRequires:    xz

BuildRequires:    kernel-abi-stablelists = %{kernel}
BuildRequires:    kernel-devel-uname-r = %{kernel}.%{_arch}

Requires:         kernel-uname-r >= %{kernel}.%{_arch}

Provides:         installonlypkg(kernel-module)
Provides:         kernel-modules >= %{kernel}.%{_arch}

Requires(posttrans): %{_sbindir}/depmod
Requires(postun):    %{_sbindir}/depmod

Requires(posttrans): %{_sbindir}/weak-modules
Requires(postun):    %{_sbindir}/weak-modules

Requires(posttrans): %{_bindir}/sort
Requires(postun):    %{_bindir}/sort


Supplements:      btrfs-progs
Recommends:       btrfs-progs

%if %{epoch}
Obsoletes:        kmod-%{pkg} < %{epoch}:
%endif
Obsoletes:        kmod-%{pkg} = %{?epoch:%{epoch}:}%{version}


%description
Btrfs is a general purpose copy-on-write filesystem with extents, writable
snapshotting, support for multiple devices and many more features focused on
fault tolerance, repair and easy administration.


%prep
%autosetup -p1 -c -T


%build
pushd src
%{__make} -C /usr/src/kernels/%{kernel}.%{_arch} %{?_smp_mflags} M=$PWD modules
popd


%install
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/fs/%{pkg} src/%{pkg}.ko

# Make .ko objects temporarily executable for automatic stripping
find %{buildroot}/lib/modules -type f -name \*.ko -exec chmod u+x \{\} \+


%clean
%{__rm} -rf %{buildroot}


%triggerin -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/fs/%{pkg}/%{pkg}.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%triggerun -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/fs/%{pkg}/%{pkg}.ko.xz" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%preun
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
rpm -ql kmod-%{pkg}-%{?epoch:%{epoch}:}%{version}-%{release}.%{_arch} | grep '/lib/modules/%{kernel}.%{_arch}/.*\.ko\.xz$' >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove


%postun
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove
    if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
    then
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules --no-initramfs
    else
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules
    fi
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%posttrans
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%files
%defattr(644,root,root,755)
/lib/modules/%{kernel}.%{_arch}
%license licenses


%changelog
* Thu May 11 2023 Kmods SIG <sig-kmods@centosproject.org> - 1:5.14.0~284.11.1-1
- kABI tracking kmod package (kernel >= 5.14.0-284.11.1.el9_2)
